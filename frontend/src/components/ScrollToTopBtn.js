// import React, { useEffect, useState } from "react";
// import { Button } from "react-bootstrap";
// import { faCircleChevronUp } from "@fortawesome/free-solid-svg-icons";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import "./ScrollToTopBtn.scss";
// export const ScrollToTopBtn = (props) => {
//   const [showGoTop, setShowGoTop] = useState(false);

//   const handleVisibleButton = () => {
//     setShowGoTop(window.pageYOffset > 50);
//   };

//   const handleScrollUp = () => {
//     window.scrollTo({ left: 0, top: 0, behavior: "smooth" });
//   };

//   useEffect(() => {
//     window.addEventListener("scroll", handleVisibleButton);
//   }, []);

//   return (
//     <div className={showGoTop ? "" : "goTopHidden"} onClick={handleScrollUp}>
//       <Button type={"button"} className="goTop">
//         <FontAwesomeIcon size="large" color="black" icon={faCircleChevronUp} />
//       </Button>
//     </div>
//   );
// };

import { Button, Card, Col, Image, Modal, Row } from "react-bootstrap";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { Grid, Pagination } from "swiper/modules";
import data from "../../data/data.json";
import { Swiper, SwiperSlide } from "swiper/react";
import "./Pictures.scss";
// Import Swiper styles
import "swiper/css";
import { useState } from "react";
// import "swiper/css/effect-cards";

// import required modules
export default function Pictures() {
  let index = 0;
  const [openModal, setOpenModal] = useState(false);
  const [showImage, setShowImage] = useState("");
  const handleClose = () => setOpenModal(false);
  const handleShow = () => setOpenModal(true);

  const catpics = data.catpics.map((catpic) => (
    <Col key={catpic.id} lg={4} md={6} sm={12} xs={12}>
      <LazyLoadImage
        onClick={handleShow}
        src={`${process.env.PUBLIC_URL}/${catpic.src}`}
        alt={catpic.alt}
        width={"100%"}
        height={400}
      />
    </Col>
  ));
  return (
    <Row id="cat-gallery" className="justify-content-center align-items-center">
      {catpics}
      {openModal ? <div></div> : ""}
      {/* {data.catpics.map((pic, index) => (
        <Col md={2}>
          <Image src={pic.src} width={300} height={300} />
        </Col>
      ))} */}

      <Modal show={openModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>preview</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </Row>
  );
}

//  <Swiper
//    slidesPerView={3}
//    grid={{
//      rows: 2,
//    }}
//    spaceBetween={30}
//    pagination={{
//      clickable: true,
//    }}
//    modules={[Grid, Pagination]}
//    className="mySwiper"
//  >
//    {data.catpics.map((pic, index) => (
//      <SwiperSlide key={index}>
//        <Image src={pic.src} width={300} height={300} />
//        {/* <Image src={pic.src} width={300} height={300} /> */}
//      </SwiperSlide>
//    ))}
//  </Swiper>;

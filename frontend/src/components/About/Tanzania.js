import "./Tanzania.scss";
import { Swiper, SwiperSlide } from "swiper/react";
import { EffectFade, Navigation, Pagination } from "swiper/modules";
import data from "../../data/data.json";
import { Card, Col, Image, Row } from "react-bootstrap";
import "swiper/css";
import "swiper/css/effect-fade";
import "swiper/css/navigation";
import "swiper/css/pagination";

import React, { useState } from "react";

export const Tanzania = () => {
  const tanzpictures = data.tanzania.map((picture) => (
    <Col sm={5} md={5} lg={4} key={picture.id}>
      <Image src={picture.src} alt={picture.alt} height={500} width={500} />
    </Col>
  ));
  const images = [
    {
      original: "images/1.jpg",
      thumbnail: "images/1.jpg",
    },
    {
      original: "images/2.jpg",
      thumbnail: "images/2.jpg",
    },
    {
      original: "images/3.jpg",
      thumbnail: "images/3.jpg",
    },
  ];
  return (
    <Row
      id="tanzania-container"
      className="justify-content-center align-items-center"
    >
      <h2>More info coming soon!</h2>
      {/* <Col md={12}>
        <h2>Tanzania</h2>
      </Col> */}
    </Row>
  );
};
{
  /* <Swiper
        slidesPerView={1}
        spaceBetween={10}
        pagination={{
          clickable: true,
        }}
        breakpoints={{
          640: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 4,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 5,
            spaceBetween: 50,
          },
        }}
        modules={[Pagination]}
        className="mySwiper"
      >
        {data.tanzania.map((pic, index) => (
          <SwiperSlide key={index} className="tanz-slide">
            <Image src={pic.src} height={500} width={600} />
          </SwiperSlide>
        ))}
      </Swiper> */
}

import { Col, Row } from "react-bootstrap";
import "./Hobbies.scss";

export const Hobbies = () => {
  return (
    <Row className="hobbies-container justify-content-center align-items-center">
      <Row className="line-row">
        <div className="line" />
      </Row>
      <Col>
        <h3>Hobbies</h3>
      </Col>
      <Col md={9}>
        <section>
          <p>
            I mostly play games, program and hang out with my cat on my free
            time. I also enjoy painting and building jigsaws. I’ve always liked
            being creative and have found and outlet for that part of myself in
            coding.
          </p>
        </section>
      </Col>
    </Row>
  );
};

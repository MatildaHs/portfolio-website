import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import "./Games.scss";
import Col from "react-bootstrap/Col";

import data from "../../data/data.json";
import useAnimationWithIntersection from "../Animations/useAnimationWithIntersection";
import { motion, useMotionValue, useTransform, animate } from "framer-motion";
import { useEffect } from "react";

const object1Variants = {
  hidden: { opacity: 0, x: 200 },
  visible: {
    opacity: 1,
    x: 0,
    transition: { duration: 1 },
  },
};

export default function Games() {
  // const count = useMotionValue(0);
  // const rounded = useTransform(count, Math.round);
  // useEffect(() => {
  //   const animation = animate(count, 100, { duration: 10 });

  //   return animation.stop;
  // }, []);

  const { ref, controls } = useAnimationWithIntersection(false);

  const containerVariants = {
    hidden: {},
    visible: { transition: { staggerChildren: 0.5 } },
  };

  const cards = data.counts.map((count, index) => (
    <Col sm={5} md={5} lg={4} key={index}>
      <motion.div variants={object1Variants}>
        <Card>
          <Card.Title>{count.label}</Card.Title>
          <Card.Subtitle>{count.hours}</Card.Subtitle>
        </Card>
      </motion.div>
    </Col>
  ));

  return (
    <motion.div
      animate={controls}
      initial="hidden"
      variants={containerVariants}
    >
      <Row className="games justify-content-center align-items-top" ref={ref}>
        <Col md={12}>
          {/* {rounded} */}
          <h2>Hours in game</h2>
        </Col>

        {cards}
      </Row>
    </motion.div>
  );
}

export const PageTitle = ({ title, subtitle }) => {
  return (
    <>
      <h1 id="page-title">{title}</h1>
      <h2 className="name">{subtitle}</h2>
    </>
  );
};
export const Subtitle = ({ subtitle }) => {
  return (
    <>
      <h2 id="name">{subtitle}</h2>
    </>
  );
};

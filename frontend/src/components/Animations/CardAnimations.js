import React, { useRef } from "react";
import { motion, useInView } from "framer-motion";
import Card from "react-bootstrap/Card";
import useAnimationWithIntersection from "./useAnimationWithIntersection";
import Image from "react-bootstrap/esm/Image";
import "./CardAnimation.scss";
const cardVariants = {
  visible: {
    opacity: 1,
    x: 0,
    transition: { duration: 1, delayChildren: 0.5, staggerChildren: 0.5 },
  },
  hidden: { opacity: 0, x: 100 },
};
const tech = {
  visible: { opacity: 1, x: 0, transition: { duration: 1 } },
  hidden: { opacity: 0, x: -500 },
};

export const AnimatedCard = ({ title, description }) => {
  const { ref, controls } = useAnimationWithIntersection();

  return (
    <motion.div
      ref={ref}
      animate={controls}
      initial="hidden"
      variants={cardVariants}
      className="square"
    >
      <Card className="job-card">
        <Card.Body>
          <Card.Subtitle>
            <h1>{title}</h1>
          </Card.Subtitle>
          <h2>{description}</h2>
        </Card.Body>
      </Card>
    </motion.div>
  );
};

export const Tech = ({ techs }) => {
  const { ref, controls } = useAnimationWithIntersection();

  return (
    <motion.div ref={ref} animate={controls} initial="hidden" variants={tech}>
      <motion.div
        // src={src}
        // width={width}
        // height={height}
        animate={controls}
        initial="hidden" // Set initial state for the img
        // Trigger the img animation
      >
        {techs}
      </motion.div>
    </motion.div>
  );
};

const container = {
  visible: {
    opacity: 1,

    transition: { duration: 1, delayChildren: 0.7, staggerChildren: 0.8 },
  },
  hidden: { opacity: 0 },
};
const item = {
  hidden: { opacity: 0, y: 200 },
  visible: {
    opacity: 1,
    y: 0,
    transition: { duration: 1 },
  },
};

export const AnimatedImage = ({ src, width, height }) => {
  const { ref, controls } = useAnimationWithIntersection();

  return (
    <motion.div
      ref={ref}
      initial="hidden"
      variants={container}
      animate={controls}
    >
      <motion.img
        src={src}
        width={width}
        height={height}
        animate={controls}
        initial="hidden" // Set initial state for the img
        // Trigger the img animation
        variants={item}
      ></motion.img>
    </motion.div>
  );
};
// const imageStart = {
//   visible: { y: 0, transition: { duration: 0 } },
//   hidden: { y: 100 },
// };

export const CardAnmte = ({ children }) => {
  const ref = useRef(null);
  const isInView = useInView(ref, { once: false });

  return (
    <section ref={ref}>
      <span
        style={{
          transform: isInView ? "none" : "translateX(-200px)",
          opacity: isInView ? 1 : 0,
          transition: "all  0.9s ease-in 0.5s",
        }}
      >
        {children}
      </span>
    </section>
  );
};

export const EducationAnmt = ({ children }) => {
  const ref = useRef(null);
  const isInView = useInView(ref, { once: true });

  return (
    <section ref={ref}>
      <span
        style={{
          transform: isInView ? "none" : "translateX(500px)",
          opacity: isInView ? 1 : 0,
          staggerChildren: 0.2,
          transition: "all 0.9s cubic-bezier(0.17, 0.55, 0.55, 1) 0.5s",
        }}
      >
        {children}
      </span>
    </section>
  );
};

export const StandardAnimation = ({ children }) => {
  const ref = useRef(null);
  const isInView = useInView(ref, { once: true });

  return (
    <section ref={ref}>
      <span
        style={{
          // transform: isInView ? "none" : "translateX(500px)",
          opacity: isInView ? 1 : 0.2,
          // staggerChildren: 0.2,
          transition: "all 0.9s cubic-bezier(0.17, 0.55, 0.55, 1) 0.5s",
        }}
      >
        {children}
      </span>
    </section>
  );
};

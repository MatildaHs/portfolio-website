import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Offcanvas from "react-bootstrap/Offcanvas";
import { faCat } from "@fortawesome/free-solid-svg-icons";
import { faHouse } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./Menu.scss";
import { useState } from "react";

import CloseButton from "react-bootstrap/esm/CloseButton";
import { NavLink } from "react-router-dom";
import { Image } from "react-bootstrap";

export const Menu = () => {
  const [show, setShow] = useState(false);

  const toggleShow = () => setShow(!show);
  // const toggleShow = () => setShow(!show);
  function handleSelect(selectedKey) {
    alert(`selected ${selectedKey}`);
  }
  return (
    <Container>
      <Navbar expand="md" id="navbar" variant="dark">
        <Navbar.Toggle
          aria-controls="offcanvasNavbar"
          id="btn-close"
          onClick={() => setShow(!show)}
        />
        <Navbar.Offcanvas
          id="offcanvasNavbar"
          aria-labelledby="offcanvasNavbarLabel"
          placement="end"
          data-bs-backdrop="true"
          show={show}
          bg="dark"
        >
          <Offcanvas.Header>
            <CloseButton onClick={toggleShow} variant="white" placement="end" />
          </Offcanvas.Header>
          <Offcanvas.Header></Offcanvas.Header>

          <Offcanvas.Body>
            <Nav variant="outlined">
              {/* <Image
                id="nav-image"
                src="./profile_picture.jpg"
                roundedCircle={true}
                width={"200px"}
                height={"200px "}
              /> */}
              <NavLink to="/">
                <FontAwesomeIcon icon={faHouse} />
              </NavLink>
              <NavLink to="/portfolio" className="link-container">
                Portfolio
              </NavLink>
              {/* <NavLink to="/about" className="link-container">
                About
              </NavLink> */}
              <NavLink to="/contact" className="link-container">
                Contact
              </NavLink>
              {/* <NavLink to="/cat">
                <FontAwesomeIcon icon={faCat} />
              </NavLink> */}
            </Nav>
          </Offcanvas.Body>
        </Navbar.Offcanvas>
      </Navbar>
    </Container>
  );
};
// style={{ color: "#ffffff" }}

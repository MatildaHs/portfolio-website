import { Card, Carousel, Col, Image, Row } from "react-bootstrap";
import data from "../../data/data.json";
import { Menu } from "../Navbar/Menu";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link, useParams } from "react-router-dom";
import "./Details.scss";
import { ScrollToTopBtn } from "../ScrollToTopBtn";
import Portfolio from "./../../pages/Portfolio";
import { PortfolioGallery } from "./PortfolioGallery";
export const Details = () => {
  return (
    <Row className="details-container justify-content-center align-items-center">
      {/* <Link to="/portfolio">
          <FontAwesomeIcon color="black" icon={faAngleLeft} size="large" />
          Back to Portfolio
        </Link> */}

      <PortfolioGallery />
      {/* Display other main details as needed */}
    </Row>
  );
};

//  const { id } = useParams();
//  const portfolioItem = data.portfolio.find((item) => item.id === parseInt(id));
//  if (!portfolioItem) {
//    return <div>Portfolio item not found.</div>;
//  }
//  const details = data.portfolio.map((detail, index) => (
//    <Col key={index}>
//      <h3>{detail.title}</h3>
//      {/* <Image
//         src={detail.imgs.src}
//         alt={detail.imgs.alt}
//         width={500}
//         height={1000}
//       /> */}
//    </Col>
//  ));
//  <div>
//    <h2>{portfolioItem.title}</h2>
//    <p>{portfolioItem.desc}</p>
//    {/* Add more details as needed */}
//    {details}
//  </div>;

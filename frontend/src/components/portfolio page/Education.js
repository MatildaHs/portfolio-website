import { Card, Col, Row } from "react-bootstrap";
import data from "../../data/data.json";
import "./Education.scss";
import { EducationCard } from "./EducationCard";
export const Education = () => {
  //   const courses = data.degrees.map((course) => (
  //     <Card key={course.id} className="education-card">
  //       <Card.Title>{course.title}</Card.Title>
  //       <Card.Body>{course.courses}</Card.Body>
  //     </Card>
  //   ));
  return (
    <Row className="education-container justify-content-center align-items-center">
      <Row>
        <Col className="h">
          <h2>Education</h2>
        </Col>
      </Row>
      <EducationCard />
    </Row>
  );
};

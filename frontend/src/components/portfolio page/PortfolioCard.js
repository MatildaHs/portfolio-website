import "./PortfolioCard.scss";
import data from "../../data/data.json";

import Col from "react-bootstrap/Col";
import { motion } from "framer-motion";

import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import { Row } from "react-bootstrap";
import useAnimationWithIntersection from "../Animations/useAnimationWithIntersection";
import { CardAnmte } from "../Animations/CardAnimations";

export const PortfolioCard = () => {
  // const { ref, controls } = useAnimationWithIntersection(true);

  const portfolios = data.portfolio.map((portfolio, index) => (
    <Col md={9} sm={9} xs={11} key={index} id={portfolio.title}>
      <CardAnmte>
        {/* <motion.div
        variants={{
          hidden: { opacity: 0 },
          visible: {
            opacity: 1,
            transition: { duration: 1, ease: "easeInOut" },
          },
        }}
        initial="hidden"
        animate={controls}
        custom={index}
        ref={ref}
      > */}
        <Card>
          <Card.Img
            src={portfolio.src}
            alt="icon"
            width={100}
            height={100}
            className={index % 2 === 1 ? "align-right" : ""}
          />
          <Card.ImgOverlay>
            <Card.Header>{portfolio.title}</Card.Header>
            <Card.Body className={index % 2 !== 1 ? "left-align" : ""}>
              <Card.Text>{portfolio.desc}</Card.Text>
              <Card.Link target="_blank" href={portfolio.link}>
                {portfolio.link}
              </Card.Link>
              <Link id="details-link" to={`/details/${portfolio.id}`}>
                Read more
              </Link>
            </Card.Body>
          </Card.ImgOverlay>
        </Card>
      </CardAnmte>
      {/* </motion.div> */}
    </Col>
  ));

  return <>{portfolios}</>;
};

// const item = {
//   hidden: { opacity: 0 },
//   visible: {
//     opacity: 1,

//     transition: { duration: 1 },
//   },
// };

// export const PortfolioCard = () => {
//   const { ref, controls } = useAnimationWithIntersection(true);

//   // Define animation variants
//   const item = {
//     hidden: { opacity: 0 },
//     visible: {
//       opacity: 1,
//       transition: { duration: 1, ease: "easeInOut" }, // Add easing
//     },
//   };

//   const portfolios = data.portfolio.map((portfolio, index) => (
//     <Col md={9} sm={9} xs={11} key={index} id={portfolio.title}>
//       <motion.div variants={item} ref={ref} animate={controls} initial="hidden">
//         <Card>
//           <Card.Img
//             src={portfolio.src}
//             alt="icon"
//             width={100}
//             height={100}
//             className={index % 2 === 1 ? "align-right" : ""}
//           />
//           <Card.ImgOverlay>
//             <Card.Header>{portfolio.title}</Card.Header>
//             <Card.Body className={index % 2 !== 1 ? "left-align" : ""}>
//               <Card.Text>{portfolio.desc}</Card.Text>
//               <Card.Link target="_blank" href={portfolio.link}>
//                 {portfolio.link}
//               </Card.Link>
//               <Link id="details-link" to={`/details/${portfolio.id}`}>
//                 Read more
//               </Link>
//             </Card.Body>
//           </Card.ImgOverlay>
//         </Card>
//       </motion.div>
//     </Col>
//   ));

//   return <>{portfolios}</>;
// };
{
  /* <Col md={9}>
        <p>
          {/* <Link target="_blank" href="https://gitlab.com/MatildaHs">
            gitlab
          </Link> */
}
//     I'm in the process of adding more projects to my portfolio. In the
//     mean time you can go to my gitlab page. More content on the way!
//   </p>
// </Col>

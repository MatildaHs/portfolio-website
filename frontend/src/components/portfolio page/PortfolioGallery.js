import { Link, useParams } from "react-router-dom";
import "./PortfolioGallery.scss";

import Portfolio from "./../../pages/Portfolio";
import data from "../../data/data.json";
import {
  Card,
  Col,
  Image,
  ListGroup,
  Modal,
  OverlayTrigger,
  Row,
  Tooltip,
} from "react-bootstrap";
import { useState } from "react";

export const PortfolioGallery = () => {
  const { id } = useParams();

  const [showModal, setShowModal] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);

  const handleCloseModal = () => {
    setShowModal(false);
    setSelectedImage(null);
  };

  const handleOpenModal = (image) => {
    setSelectedImage(image);
    setShowModal(true);
  };

  const mapPortfolio = data.portfolio.map((portfolio) => (
    <ListGroup.Item key={portfolio.id}>
      <Link to={`/details/${portfolio.id}`}>{portfolio.title}</Link>
    </ListGroup.Item>
  ));

  const portfolioItem = data.portfolio.find((item) => item.id === parseInt(id));
  if (!portfolioItem) {
    return <div>Portfolio item not found.</div>;
  }

  // const goalLists = portfolioItem.details.map((liObj, idx) => {
  //   <ul>
  //     {liObj.goals.map((goal, idx) => (
  //       <li key={idx}>{goal.goal}</li>
  //     ))}
  //   </ul>;
  // });
  const details = portfolioItem.details.map((detail, index) => (
    <>
      <Row className="head jusify-content-center align-items-center">
        <Col md={6} xs={12}>
          <h1>{portfolioItem.title}</h1>{" "}
          <Col id="tech" md={12}>
            {detail.techs.map((tech, index) => (
              <OverlayTrigger
                key={index}
                overlay={<Tooltip>{tech.tooltip}</Tooltip>}
              >
                <Image
                  src={tech.src}
                  alt={tech.tooltip}
                  width={75}
                  height={75}
                />
              </OverlayTrigger>
            ))}
          </Col>
        </Col>
        <Col xs={10} sm={11} md={6} key={index}>
          <h2>{detail.longDesc}</h2>
        </Col>
      </Row>
      <Row id="goals" className="justify-content-center align-items-center">
        <ul>
          {detail.goals.map((goal, idx) => (
            <li key={idx}>{goal.goal}</li>
          ))}
        </ul>
      </Row>
      {detail.imgs.map((image, idx) => (
        <Col md={3} sm={4} key={idx}>
          <Card
            id="gallery-card"
            className="bg-dark text-white"
            onClick={() => handleOpenModal(image)}
          >
            <Card.Img
              src={image.src}
              alt={image.alt}
              width={100}
              height={200}
            />

            <Card.Title>{image.alt}</Card.Title>
          </Card>
        </Col>
      ))}
      {/* <ScrollToTopBtn />

      <ScrollToTopBtn /> */}
      <Col md={12} style={{ marginTop: "3rem" }}>
        <Link target="_blank" to={portfolioItem.link}>
          {portfolioItem.link}
        </Link>
      </Col>
    </>
  ));
  {
  }
  return (
    <Row className="gallery-container justify-content-center align-items-center">
      {/* <Link to="/portfolio">
          <FontAwesomeIcon color="black" icon={faAngleLeft} size="large" />
          Back to Portfolio
        </Link> */}

      {details}

      <Modal
        id="portfolio-modal"
        show={showModal}
        onHide={handleCloseModal}
        centered
        size="xl"
      >
        <Modal.Header closeButton />
        <Modal.Body>
          {selectedImage && (
            <Image
              src={selectedImage.src}
              alt={selectedImage.alt}
              className="img-fluid"
            />
          )}
        </Modal.Body>
      </Modal>

      <Col md={10} id="other">
        <h3>Other projects</h3>
        <ListGroup horizontal>{mapPortfolio}</ListGroup>
      </Col>
      {/* Display other main details as needed */}
    </Row>
  );
};

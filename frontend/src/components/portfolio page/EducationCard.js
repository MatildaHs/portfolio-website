import { Card, Col, ListGroup, Row } from "react-bootstrap";
import "./EducationCard.scss";
import data from "../../data/data.json";
import Header from "./../Header";
import { CardAnmte, EducationAnmt } from "../Animations/CardAnimations";
import { motion } from "framer-motion";
const variants = {
  open: { transform: "translatex(0)" },
  closed: { transform: "translatex(200px)" },
};
export const EducationCard = () => {
  const courses = data.degrees.map((degree, index) => (
    <Row
      key={index}
      className="justify-content-center"
      style={{ marginTop: "5rem" }}
    >
      <Col lg={2}>
        <h3> {degree.year}</h3>
      </Col>
      <Col lg={7} md={9} sm={10} xs={12}>
        <CardAnmte>
          <Card className="education-card">
            <Card.Header>
              <div className="circle" />
            </Card.Header>
            <Card.Title>{degree.title}</Card.Title>
            <Card.Body>
              <ListGroup as="ul">
                {degree.courses.map((courseName, idx) => (
                  <EducationAnmt>
                    <ListGroup.Item as="li" key={idx}>
                      {courseName.name}
                    </ListGroup.Item>
                  </EducationAnmt>
                ))}
              </ListGroup>
            </Card.Body>
          </Card>
        </CardAnmte>
      </Col>
    </Row>
  ));
  return <>{courses}</>;
};

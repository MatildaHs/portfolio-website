// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
// import "swiper/css/effect-cards";
import data from "../../data/data.json";
import "./styles.scss";
import { Autoplay, Pagination } from "swiper/modules";
// import required modules

import { Card } from "react-bootstrap";

export default function PortfolioSwiper() {
  return (
    <Swiper
      pagination={{
        dynamicBullets: true,
      }}
      modules={[Autoplay, Pagination]}
      loop={true}
      className="mySwiper"
      autoplay={{
        delay: 2500,
        disableOnInteraction: false,
      }}
    >
      {data.portfolio.map((portfolio, index) => (
        <SwiperSlide>
          <Card style={{ width: "30rem" }}>
            {/* <Card.Link href={`{#/${portfolio.title}`}> */}
            <Card.ImgOverlay>
              <Card.Body>
                <Card.Title>{portfolio.title}</Card.Title>
              </Card.Body>
            </Card.ImgOverlay>
            <Card.Img variant="bottom" src={portfolio.src} alt="icon" />
            {/* </Card.Link> */}
          </Card>
        </SwiperSlide>
      ))}
    </Swiper>
  );
}

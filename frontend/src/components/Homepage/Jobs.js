import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import "./Jobs.scss";

import Col from "react-bootstrap/Col";

export default function Jobs() {
  return (
    <Row id="jobs" className="justify-content-space-between align-items-top">
      <Row className="line-row">
        <div className="line" />
      </Row>
      <Col xs={10} md={4}>
        <Card className="job-card">
          <Card.Body>
            <Card.Subtitle>
              <h3>Frontend</h3>
            </Card.Subtitle>
            <Card.Text>
              Frontend I started my journey to become a frontend developer while
              studying Java
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={10} md={4}>
        <Card className="job-card">
          <Card.Body>
            <Card.Subtitle>
              <h3>Ux/Ui-design</h3>
            </Card.Subtitle>
            <Card.Text>
              I have also taken courses in Ux/UI design where I learned about
              the work process as a UX/UI-designer.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col sm={10} md={4}>
        <Card className="job-card">
          <Card.Body>
            <Card.Subtitle>
              <h3>Web development</h3>
            </Card.Subtitle>
            <Card.Text>
              Designing and buildning websites is incredibly fun and something i
              love to to
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

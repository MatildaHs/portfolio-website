import Row from "react-bootstrap/Row";
import "./Hire.scss";

import Col from "react-bootstrap/Col";
import { motion } from "framer-motion";

import useAnimationWithIntersection from "../Animations/useAnimationWithIntersection";
import { NavLink } from "react-bootstrap";

const object1Variants = {
  hidden: { opacity: 0, x: 200 },
  visible: {
    opacity: 1,
    x: 0,
    transition: { duration: 0.5 },
  },
};

const object2Variants = {
  hidden: { opacity: 0, x: -300 },
  visible: { opacity: 1, x: 0, transition: { duration: 0.5 } },
};

export default function Hire() {
  const { ref, controls } = useAnimationWithIntersection(false);
  const containerVariants = {
    hidden: {},
    visible: { transition: { staggerChildren: 0.5, delayChildren: 0.5 } }, // Stagger the children animations
  };

  return (
    <motion.div
      animate={controls}
      initial="hidden"
      variants={containerVariants}
    >
      <Row className="hire justify-content-center align-items-center" ref={ref}>
        <Col md={3}>
          <motion.div
            className="border-container-start"
            variants={object2Variants}
          ></motion.div>
        </Col>
        <Col md={6} sm={11} xs={11}>
          <h1>Want to hire me?</h1>
          <NavLink href="/contact">GO TO CONTACT INFO</NavLink>
        </Col>

        <Col md={3} className="">
          <motion.div
            className="border-container"
            variants={object1Variants}
          ></motion.div>
        </Col>
      </Row>
    </motion.div>
  );
}

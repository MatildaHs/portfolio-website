import Row from "react-bootstrap/Row";
import Image from "react-bootstrap/Image";

import { faQuoteLeft } from "@fortawesome/free-solid-svg-icons";
import Col from "react-bootstrap/Col";
import "./AboutMe.scss";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { NavLink } from "react-bootstrap";
import { StandardAnimation } from "../Animations/CardAnimations";
export default function AboutMe() {
  return (
    <Row
      id="about-me-container"
      className="justify-content-center align-items-center"
    >
      <Col md={4} id="head">
        <h1>ABOUT ME</h1>
      </Col>
      <Col lg={6} md={6} sm={11} xs={11} id="profile-picture">
        <Image
          src={process.env.PUBLIC_URL + "/images/profile_picture.jpg"}
          loading="lazy"
          style={{ width: "25em", height: "400px" }}
        />
      </Col>
      <Col md={10} id="quote" className="justify-content-start">
        <FontAwesomeIcon icon={faQuoteLeft} size="3x" />
      </Col>
      <Col md={8} lg={9} sm={11} xs={11} className="about-text">
        <section>
          <p>
            I have been studying to become a developer for 4 years. I discovered
            programming when I took a course in C# and haven´t looked back
            since. I have tried a couple of different developer roles and
            ultimately found Frontend and UX/UI-design. I love to combine the
            creative work of design with the logic aspect of buildning it in
            code.
          </p>
        </section>
      </Col>
      <Col md={8} lg={9} sm={11} xs={11} className="about-text">
        <section>
          <h3>Hobbies</h3>
          <p>
            I mostly play games, program and hang out with my cat on my free
            time. I also enjoy painting and building jigsaws. I’ve always liked
            being creative and have found and outlet for that part of myself in
            coding.
          </p>
        </section>
      </Col>
      <Col md={10}>
        <NavLink
          id="resume"
          href={require("../../documents/CV_MATILDA_SCHÜLER_SWE.pdf")}
          target="_blank"
        >
          Resumé
        </NavLink>
      </Col>
    </Row>
  );
}

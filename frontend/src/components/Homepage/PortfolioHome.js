import Row from "react-bootstrap/Row";
import "./PortfolioHome.scss";
import Stack from "react-bootstrap/Stack";
import Col from "react-bootstrap/Col";
import { motion } from "framer-motion";

import Card from "react-bootstrap/Card";
import { PortfolioCard } from "../portfolio page/PortfolioCard";
import useAnimationWithIntersection from "../Animations/useAnimationWithIntersection";
import data from "../../data/data.json";
import { Swiper, SwiperSlide } from "swiper/react";
import PortfolioSwiper from "../Swipers/PortfolioSwiper";
import { Container, Image } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";

const container = {
  visible: {
    opacity: 1,

    transition: { duration: 1, staggerChildren: 0.5 },
  },
  hidden: { opacity: 1 },
};

const itemHeader = {
  hidden: { opacity: 0, x: 700 },
  visible: {
    opacity: 1,
    x: 0,
    transition: { duration: 1, delay: 1 },
  },
};

export default function PortfolioHome() {
  const { ref, controls } = useAnimationWithIntersection(true);
  const containerVariants = {
    hidden: {},
    visible: { transition: { staggerChildren: 0.5 } },
  };
  return (
    <Container fluid className="home-portfolio">
      <Row className="portfolio-container justify-content-center align-items-center">
        <Col md={11} lg={5} sm={10} xs={12} className="justify-content-center">
          <Link to="/portfolio" className="header">
            <h2>Go to portfolio</h2>
            {/* <FontAwesomeIcon
              icon={faArrowRight}
              style={{ color: "#ffffff" }}
              size="4x"
            /> */}
          </Link>
        </Col>
        <Col lg={7} md={12} sm={11} xs={11} className="card-container">
          <Card id="first">
            <Card.Img src={data.portfolio[0].src} />
          </Card>

          <Card id="second">
            <Card.Img src={data.portfolio[1].src} />
          </Card>
          <Card id="third">
            <Card.Img src={data.portfolio[2].src} />
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
{
  /* <Row>
        <Col className="h">
          <motion.div variants={itemHeader}>
            <h1>portfolio</h1>
          </motion.div>
        </Col>
      </Row> */
}
{
  // <motion.div className="home-portfolio">
  //   <Row
  //     className="portfolio-container justify-content-center align-items-center"
  //     ref={ref}
  //   >
  //     <Col md={6}>
  //       <Link to="/portfolio" className="link">
  //         See all projects
  //       </Link>
  //     </Col>
  //     <Col md={6}>
  //       <PortfolioSwiper />
  //     </Col>
  //   </Row>
  // </motion.div>
}
{
  /* <div className="first"> */
}

{
  /* <div className="second">
              <Image src={data.portfolio[1].src} width={300} height={400} />
            </div>
          </div>
          <div className="third">
            <Image src={data.portfolio[2].src} width={300} height={400} />
          </div> */
}

{
  /* <Row className="line-row">
          <div className="line" />
        </Row> */
}

// Import Swiper React components

import Col from "react-bootstrap/esm/Col";
import data from "../../data/data.json";
import "./TechStacks.scss";
import Image from "react-bootstrap/Image";

import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import { motion } from "framer-motion";

import useAnimationWithIntersection from "../Animations/useAnimationWithIntersection";

import SwiperCore from "swiper/core";
import { Navigation, Pagination } from "swiper/modules";

SwiperCore.use([Pagination, Navigation]); // Import Swiper styles
const tech = {
  visible: { opacity: 1, x: "0%", transition: { duration: 1 } },
  hidden: { opacity: 0, x: "100%" },
};
export const TechStacks = () => {
  const { ref, controls } = useAnimationWithIntersection(true);
  const containerVariants = {
    visible: { transition: { staggerChildren: 0.5 } }, // Stagger the children animations
    hidden: {},
  };
  const techs = data.techstacks.map((tech) => (
    <OverlayTrigger key={tech.id} overlay={<Tooltip>{tech.tooltip}</Tooltip>}>
      <Image src={tech.src} alt={tech.tooltip} width={75} height={75} />
    </OverlayTrigger>
  ));
  return (
    <motion.div
      animate={controls}
      initial="hidden"
      variants={containerVariants}
    >
      <Col className="tech-container " ref={ref}>
        <motion.div variants={tech}>{techs}</motion.div>
      </Col>
    </motion.div>
  );
};
// export const TechStacks = () => {
//   // const techs = data.techstacks.map((tech) => (
//   //   <OverlayTrigger key={tech.id} overlay={<Tooltip>{tech.tooltip}</Tooltip>}>
//   //     <Image src={tech.src} alt={tech.tooltip} width={75} height={75} />
//   //   </OverlayTrigger>
//   // ));

//   return (
//     <>
//       <Swiper
//         // slidesPerView={1}

//         // slidesPerGroup={3}
//         loop={true}
//         slidesPerView={13}
//         spaceBetween={2}
//         pagination={{
//           clickable: true,
//         }}
//         breakpoints={{
//           640: {
//             slidesPerView: 2,
//             spaceBetween: 20,
//           },
//           768: {
//             slidesPerView: 4,
//             spaceBetween: 40,
//           },
//           1024: {
//             slidesPerView: 5,
//             spaceBetween: 50,
//           },
//         }}
//         navigation={true}
//       >
//         {data.techstacks.map((tech) => (
//           <SwiperSlide width={100}>
//             <OverlayTrigger
//               key={tech.id}
//               overlay={<Tooltip>{tech.tooltip}</Tooltip>}
//             >
//               <Image src={tech.src} alt={tech.tooltip} width={75} height={75} />
//             </OverlayTrigger>
//           </SwiperSlide>
//         ))}
//       </Swiper>
//     </>
//   );
// };

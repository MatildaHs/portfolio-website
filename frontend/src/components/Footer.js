import Row from "react-bootstrap/Row";
import "./Footer.scss";
import Col from "react-bootstrap/Col";
import { Nav } from "react-bootstrap";
import useAnimationWithIntersection from "./Animations/useAnimationWithIntersection";
import { Link } from "react-router-dom";
import { Contact } from "./../pages/Contact";

const object1Variants = {
  hidden: { opacity: 0, x: 200 },
  visible: {
    opacity: 1,
    x: 0,
    transition: { duration: 1 },
  },
};

const object2Variants = {
  hidden: { opacity: 0, x: -200 },
  visible: { opacity: 1, x: 0, transition: { duration: 1 } },
};

export default function Footer() {
  const { ref, controls } = useAnimationWithIntersection(false);

  const containerVariants = {
    hidden: {},
    visible: {}, // Stagger the children animations
  };

  return (
    <Row
      className="footer justify-content-center align-items-flex-start"
      aria-label="footer"
      role="footer"
    >
      <Row md={11} className="line-row">
        <div className="line" />
      </Row>
      <Nav>
        <Link to="/portfolio">Portfolio</Link>

        {/* <Link to="/about">About</Link> */}

        <Link to="/contact">Contact</Link>
        <a
          href={require("../documents/CV_MATILDA_SCHÜLER_SWE.pdf")}
          target="_blank"
        >
          RESUMÉ
        </a>
        {/* <Link to="/pdf/CV_MATILDA_SCHÜLER.pdf">CV</Link> */}
      </Nav>

      <Row md={11} className="line-row">
        <div className="line" />
      </Row>
      <Col md={4}>
        <p id="contact">Email: m.h.schyler@gmail.com</p>
      </Col>
      <Col md={4}>
        <p id="contact"> Tel: 0706-818787</p>
      </Col>
      <Col md={12} className="copyright">
        <h5>Copyright Matilda Schüler 2023</h5>
      </Col>
    </Row>
  );
}

import { useState, useEffect } from "react";
import Row from "react-bootstrap/Row";
import "./Header.scss";
import Col from "react-bootstrap/Col";
import { motion } from "framer-motion";
import { Menu } from "./Navbar/Menu";
import useAnimationWithIntersection from "./Animations/useAnimationWithIntersection";
import { usePageTitle } from "../hooks/UsePageTitle";
import { PageTitle, Subtitle } from "./PageTitle";

export default function Header({ title, subtitle }) {
  const { ref, controls } = useAnimationWithIntersection(false);
  const [users, setUsers] = useState([]);

  const cardVariants = {
    visible: {
      opacity: 1,
      x: 0,
      transition: { duration: 1, delayChildren: 0.5, staggerChildren: 0.5 },
    },
    hidden: { opacity: 0, x: 100 },
  };
  // useEffect(() => {
  //   fetch("http://localhost:8000/users")
  //     .then((res) => res.json())
  //     .then((data) => {
  //       setUsers(data);
  //     });
  // }, []);

  return (
    <Row id="header-container" className="justify-content-end align-items-end">
      <Menu />
      <Col md={8} sm={12} xs={12} className="justify-self-start">
        <PageTitle title={title} />
        {/* <h1 id="hello">Home</h1> */}
      </Col>

      <Col sm={12} xs={12} md={6} className="align-self-start">
        <motion.div
          variants={cardVariants}
          ref={ref}
          animate={controls}
          initial="hidden"
        >
          <Subtitle subtitle={subtitle} />
        </motion.div>

        <Row className="line-row">
          <div className="line" />
        </Row>
      </Col>
    </Row>
  );
}

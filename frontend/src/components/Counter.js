import React, { useEffect } from "react";
import { useMotionValue, useTransform, animate } from "framer-motion";

export const Counter = ({ label, number }) => {
  const count = useMotionValue(0);
  const rounded = useTransform(count, Math.round);

  useEffect(() => {
    const end = parseFloat(number);
    // const totalMilSecDur = parseFloat(duration) * 1000;
    const animation = animate(count, end, { duration: 2 });

    return () => animation.stop();
  }, [number]);

  return (
    <h3>
      {console.log({ rounded })}
      <i>
        {label}: {rounded.get()}
      </i>
    </h3>
  );
};

//   const [count, setCount] = useState("0");

//   useEffect(() => {
//     let start = 0;
//     const end = parseInt(number.substring(0, 3));

//     if (start === end) return;

//     let totalMilSecDur = parseInt(duration);
//     let incrementTime = (totalMilSecDur / end) * 1000;

//     let timer = setInterval(() => {
//       start += 1;
//       setCount(String(start) + number.substring(3));
//       if (start === end) clearInterval(timer);
//     }, incrementTime);
//   }, [number, duration]);

//   );

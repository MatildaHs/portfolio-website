import "./App.css";
import Container from "react-bootstrap/Container";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Homepage from "./pages/Homepage";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Portfolio from "./pages/Portfolio";
import { Contact } from "./pages/Contact";
import Catpage from "./pages/Catpage";
import ScrollToTop from "./hooks/SmoothScroll";
import SmoothScroll from "./hooks/SmoothScroll";
import { About } from "./pages/About";
import { DetailsPage } from "./pages/DetailsPage";
import ScrollToTopOnMount from "./hooks/ScrollToTopOnMount";

function App() {
  return (
    <BrowserRouter>
      <SmoothScroll>
        <Container fluid className="App">
          <Routes>
            <Route path="/" element={<Homepage />} />
            <Route path="/portfolio" element={<Portfolio />} />
            <Route path="/about" element={<About />} />
            <Route path="/contact" element={<Contact />} />
            <Route path="/cat" element={<Catpage />} />
            <Route path="/details/:id" element={<DetailsPage />} />
          </Routes>
          <Footer />
        </Container>
      </SmoothScroll>
    </BrowserRouter>
  );
}

export default App;

import { Row } from "react-bootstrap";
import { usePageTitle } from "../hooks/UsePageTitle";
import Header from "../components/Header";
import "./About.scss";
import Games from "../components/About/Games";
import { Hobbies } from "../components/About/Hobbies";
import { Tanzania } from "../components/About/Tanzania";
import Pictures from "../components/Catpage/Pictures";
export const About = (title) => {
  usePageTitle("About me");
  return (
    <>
      <Header title="About me" subtitle="a bit more about me" />
      <Row className="about-me justify-content-center align-items-center">
        <Hobbies /> <Games />
        <Pictures />
        <Tanzania />
      </Row>
    </>
  );
};

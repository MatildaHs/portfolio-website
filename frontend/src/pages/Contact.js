import { Col, Row } from "react-bootstrap";
import data from "../data/data.json";
import "./Contact.scss";
import { faPhone, faEnvelope } from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGitlab } from "@fortawesome/fontawesome-svg-core";
import { Link } from "react-router-dom";
import Header from "../components/Header";
import { usePageTitle } from "../hooks/UsePageTitle";

export const Contact = () => {
  usePageTitle("Contact ");
  return (
    <>
      <Header title="Contacts" />
      <Row className="contact-info justify-content-space-between align-items-center">
        {/* <h1>Contact info</h1> */}
        <Col xs={12} sm={6}>
          <FontAwesomeIcon
            icon={faEnvelope}
            style={{ color: "#000000", margin: "10px" }}
            size="2x"
          />
          <h3>{data.contact.email}</h3>
        </Col>
        <Col>
          <FontAwesomeIcon
            icon={faPhone}
            style={{ color: "#000000", margin: "10px" }}
            size="2x"
          />
          <h3>{data.contact.tel}</h3>
        </Col>
        {/* <Col md={12} xs={12}>
          <a target="_blank" href={data.contact.gitlab}>
            {data.contact.gitlab}
          </a>
        </Col> */}
        <Col md={12} xs={12}>
          <h3>Gitlab:</h3>
          <a target="_blank" href={data.contact.school}>
            {data.contact.school}
          </a>
        </Col>
      </Row>
    </>
  );
};

import "./Homepage.css";

import AboutMe from "../components/Homepage/AboutMe";
import Jobs from "../components/Homepage/Jobs";

import Hire from "../components/Homepage/Hire";

import { TechStacks } from "../components/Homepage/TechStacks";

import PortfolioHome from "../components/Homepage/PortfolioHome";
import { usePageTitle } from "../hooks/UsePageTitle";
import Header from "../components/Header";
import Games from "../components/About/Games";
import { StandardAnimation } from "../components/Animations/CardAnimations";

export default function Homepage() {
  usePageTitle("Homepage");
  // const [users, setUsers] = useState([]);

  // useEffect(() => {
  //   fetch("http://localhost:8000/users")
  //     .then((res) => res.json())
  //     .then((data) => {
  //       setUsers(data);
  //     });
  // }, []);

  return (
    <>
      <Header title="hello" subtitle="My name is Matilda" />
      <StandardAnimation>
        <Jobs />
      </StandardAnimation>
      <StandardAnimation>
        <Games />
      </StandardAnimation>
      <StandardAnimation>
        <AboutMe />
      </StandardAnimation>
      <StandardAnimation>
        <TechStacks />
      </StandardAnimation>
      <StandardAnimation>
        <PortfolioHome />
      </StandardAnimation>
      <Hire />
    </>
  );
}

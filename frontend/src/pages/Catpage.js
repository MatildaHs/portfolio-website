import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import "./Catpage.scss";
import Image from "react-bootstrap/Image";
import Col from "react-bootstrap/Col";
import Header from "../components/Header";
import { usePageTitle } from "../hooks/UsePageTitle";
import Pictures from "../components/Catpage/Pictures";

export default function Catpage() {
  usePageTitle("My Cat");
  return (
    <>
      <Header title="My cat" subtitle="Just some cat pictures" />
      <Pictures />
      {/* <Row
        id="cat-gallery"
        className="justify-content-center align-items-center"
      >
        <Col>
          <Image
            src="kitty.jpg"
            alt="cat profile"
            height="300"
            width="350"
            id="pic"
          />
        </Col>
        <Col>
          <Image
            src="IMG_5065.jpg"
            alt="cute cat"
            height="300"
            width="350"
            id="pic"
          />
        </Col>
        <Col>
          <Image
            src="IMG_5345.jpg"
            alt="cuter cat"
            height="300"
            width="350"
            id="pic"
          />
        </Col>
      </Row> */}
    </>
  );
}

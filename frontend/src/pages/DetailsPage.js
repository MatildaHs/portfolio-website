import Header from "../components/Header.js";
import { Menu } from "../components/Navbar/Menu";
import { Details } from "../components/portfolio page/Details";
import { PortfolioGallery } from "../components/portfolio page/PortfolioGallery.js";
import Test from "../data/Test.js";

export const DetailsPage = () => {
  return (
    <>
      <Menu />
      <PortfolioGallery />
    </>
  );
};

import Row from "react-bootstrap/Row";
import "./Portfolio.scss";

import Col from "react-bootstrap/Col";
import { motion } from "framer-motion";

import useAnimationWithIntersection from "../components/Animations/useAnimationWithIntersection";

import Header from "../components/Header";
import { usePageTitle } from "../hooks/UsePageTitle";
import { PortfolioCard } from "../components/portfolio page/PortfolioCard";
import { Education } from "../components/portfolio page/Education";
import { Container } from "react-bootstrap";

// const container = {
//   visible: {
//     opacity: 1,

//     transition: { duration: 1, staggerChildren: 0.5 },
//   },
//   hidden: { opacity: 1 },
// };

// const itemHeader = {
//   hidden: { opacity: 0, x: 700 },
//   visible: {
//     opacity: 1,
//     x: 0,
//     transition: { duration: 1, delay: 1 },
//   },
// };

export default function Portfolio() {
  usePageTitle("Portfolio");
  const { ref, controls } = useAnimationWithIntersection(true);

  return (
    <Row className="portfolio-container">
      <Header title="Portfolio" subtitle="past and current projects" />
      {/* <motion.div
        id="portfolio"
        animate={controls}
        // initial="hidden"
        variants={container}
      > */}
      <Row
        className="overlay justify-content-center align-items-center"
        // ref={ref}
      >
        <PortfolioCard />

        {/* <Row className="line-row">
          <div className="line" />
        </Row> */}
      </Row>
      {/* </motion.div> */}
      <Education />
    </Row>
  );
}

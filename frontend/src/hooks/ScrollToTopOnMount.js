// import React, { useEffect } from "react";
// import { useHistory } from "react-router-dom";

// const ScrollToTopOnMount = () => {
//   const history = useHistory();

//   useEffect(() => {
//     // Scroll to the top when the component mounts without animation
//     window.scrollTo({ top: 0, behavior: "auto" });

//     // Subscribe to history changes to scroll to top on route change
//     const unlisten = history.listen(() => {
//       window.scrollTo({ top: 0, behavior: "auto" });
//     });

//     // Clean up the listener when the component unmounts
//     return () => {
//       unlisten();
//     };
//   }, [history]);

//   return null; // This component doesn't render anything
// };

// export default ScrollToTopOnMount;
